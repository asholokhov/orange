
"use strict";

var fs = require('fs'),
    app = require('express')(),
    registry = require('./app/classes/registry'),
    passport = require('passport'),
    config = require('./config/config')[app.get('env')],
    winston = require('winston'),
    port = process.env.PORT || config.defaultPort;

/* Set registry params */
registry.set('env', app.get('env'));
registry.set('config', require('./config/config')[registry.get('env')]);

/* Bootstrap i18n */
require('./config/i18n');

/* Boostrap DB */
require('./config/db');

/* Bootstrap logger config */
require('./config/winston');

/* bootstrap passport config */
require('./config/passport')(passport);

/* express settings */
require('./config/express')(app, config);

/* Start the app by listening on <port> */
app.listen(port);

/* Чтобы приложенька не падала */
process.on('uncaughtException', function (err) {
    winston.info(err.stack);
});

console.log('env:' + registry.get('env'));
console.log('Express app started on port ' + port);

// expose app
module.exports = app;
