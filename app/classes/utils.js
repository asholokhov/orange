/**
 * Created by ivan on 7/21/14.
 */

"use strict";

module.exports = {

    isObjectIdValid: function (objectId) {
        return (/^[0-9a-fA-F]{24}$/).test(objectId);
    }

};