
"use strict";

var _           = require('underscore'),
    acl         = require('../../../config/acl'),
    mongoose    = require('mongoose'),
    Contest     = mongoose.model('Contest'),
    router      = require('express').Router();

router.get('/', function (req, res) {

    res.render('admin/main');

});

module.exports = router;