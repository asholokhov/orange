
"use strict";

var acl         = require('../../../config/acl'),
    router      = require('express').Router(),
    mongoose    = require('mongoose'),
    logger      = require('winston'),
    Problem     = mongoose.model('Problem');

// Если id корректный - передать, если нет - к следующему роуту
router.param('id', function (req, res, next, id) {
    next(mongoose.Types.ObjectId.isValid(id) ? null : 'route');
});


router.get('/', function (req, res) {

    Problem.find().sort('-dateCreated').exec(function (err, problems) {
        if ( err ) { throw err; }

        res.render('admin/problems/index', {
            problems: problems
        });

    });

});

router.get('/new', acl.check('problem', ['view', 'edit']), function (req, res) {

    res.render('admin/problems/edit');

});

router.get('/:id/edit', acl.check('problem', ['view', 'edit']), function (req, res, next) {

    if ( mongoose.Types.ObjectId.isValid(req.params.id) ) {

        Problem.findById(req.params.id, function(err, problem) {
            if ( err ) { throw err; }

            res.render('admin/problems/edit', {
                problem: problem
            });
        });

    } else { next('route'); }

});

router.post('/:id', acl.check('problem', ['view', 'edit']), function (req, res) {

    Problem.findById(req.params.id, function (err, problem) {
        if ( err ) { throw err; }

        if ( problem ) {

            problem.setName(req.body.name);
            problem.setIOFilename(req.body.iofilename);
            problem.setTimeLimit(req.body.timeLimit);
            problem.setMemoryLimit(req.body.memoryLimit);
            problem.setText(req.body.text);
            problem.setActive(req.body.active);

            problem.save(function (err, problem) {
                if ( err ) { throw err; }

                if ( req.files.tests ) {

                    problem.updateTests(req.files.tests.path, function (err) {
                        if ( err ) { throw err; }

                        req.flash('info', 'Тесты к задаче добавлены успешно!');

                    });

                }

                res.redirect('/admin/problems');

            });

        } else {
            res.redirect('/admin/problems');
        }


    });

});

router.post('/', acl.check('problem', ['view', 'edit']), function (req, res) {

    var newProblem = new Problem();

    newProblem.setName(req.body.name);
    newProblem.setIOFilename(req.body.iofilename);
    newProblem.setTimeLimit(req.body.timeLimit);
    newProblem.setMemoryLimit(req.body.memoryLimit);
    newProblem.setText(req.body.text);
    newProblem.setActive(req.body.active);

    newProblem.save(function (err, problem) {
        if ( err ) { throw err; }

        if ( req.files.tests ) {

            problem.updateTests(req.files.tests.path, function (err) {
                if ( err ) { throw err; }

                req.flash('info', 'Тесты к задаче добавлены успешно!');

            });

        }

        res.redirect('/admin/problems');

    });

});


router.get('/:id/publish', acl.check('problem', ['view', 'edit']), function (req, res) {

    Problem.findById(req.params.id, function (err, problem) {
        if ( err ) { throw err; }

        if ( problem ) {

            problem.setActive(true);

            problem.save(function (err, problem) {

                req.flash('info', 'Задача ' + problem.getName() + ' успешно опубликована');
                res.redirect('/admin/problems');

            });

        } else {
            throw new Error('Задачи с таким id не существует!');
        }

    });

});

router.get('/:id/unpublish', acl.check('problem', ['view', 'edit']), function (req, res) {

    Problem.findById(req.params.id, function (err, problem) {
        if ( err ) { throw err; }

        if ( problem ) {

            problem.setActive(false);

            problem.save(function (err, problem) {

                req.flash('info', 'Задача ' + problem.getName() + ' успешно снята с публикации');
                res.redirect('/admin/problems');

            });

        } else {
            throw new Error('Задачи с таким id не существует!');
        }

    });

});

module.exports = router;