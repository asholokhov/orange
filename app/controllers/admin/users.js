
"use strict";

var acl         = require('../../../config/acl'),
    router      = require('express').Router(),
    mongoose    = require('mongoose'),
    logger      = require('winston'),
    User        = mongoose.model('User');

// Если id корректный - передать, если нет - к следующему роуту
router.param('id', function (req, res, next, id) {
    next(mongoose.Types.ObjectId.isValid(id) ? null : 'route');
});


router.get('/', function (req, res) {

    User.find().sort('-dateCreated').exec(function (err, users) {
        if ( err ) { throw err; }

        res.render('admin/users/index', {
            users: users
        });

    });

});

router.get('/new', acl.check('user', ['view', 'edit']), function (req, res) {

    res.render('admin/users/edit');

});

router.get('/:id/edit', acl.check('user', ['view', 'edit']), function (req, res, next) {

    if ( mongoose.Types.ObjectId.isValid(req.params.id) ) {

        User.findById(req.params.id, function(err, user) {
            if ( err ) { throw err; }

            res.render('admin/users/edit', {
                userEdit: user
            });
        });

    } else { next('route'); }

});

router.post('/:id', acl.check('user', ['view', 'edit']), function (req, res) {

    User.findById(req.params.id, function (err, user) {
        if ( err ) { throw err; }

        if ( user ) {

            user.setFname(req.body.fname);
            user.setLname(req.body.lname);
            user.setGroup(req.body.group);
            user.setEmail(req.body.email);
            user.setPass(req.body.pass);
            user.setActive(req.body.active);
            user.setAdmin(req.body.admin);

            user.save(function (err, user) {
                if ( err ) { throw err; }

                req.flash('info', 'Пользователь ' + user.getFullName() + ' успешно обновлен');
                res.redirect('/admin/users');

            });

        } else { res.render404(); }


    });

});

router.post('/', acl.check('user', ['view', 'edit']), function (req, res) {

    var user = new User();

    user.setFname(req.body.fname);
    user.setLname(req.body.lname);
    user.setGroup(req.body.group);
    user.setEmail(req.body.email);
    user.setPass(req.body.pass);
    user.setActive(req.body.active);
    user.setAdmin(req.body.admin);

    user.save(function (err, user) {
        if ( err ) { throw err; }

        req.flash('info', 'Пользователь ' + user.getFullName() + ' успешно создан');
        res.redirect('/admin/users');

    });
});


router.get('/:id/publish', acl.check('user', ['view', 'edit']), function (req, res) {

    User.findById(req.params.id, function (err, user) {
        if ( err ) { throw err; }

        if ( user ) {

            user.setActive(true);

            user.save(function (err, user) {

                req.flash('info', 'Пользователь ' + user.getFullName() + ' успешно активирован');
                res.redirect('/admin/users');

            });

        } else {
            res.render404();
        }

    });

});

router.get('/:id/unpublish', acl.check('user', ['view', 'edit']), function (req, res) {

    User.findById(req.params.id, function (err, user) {
        if ( err ) { throw err; }

        if ( user ) {

            user.setActive(false);

            user.save(function (err, user) {

                req.flash('info', 'Пользователь ' + user.getFullName() + ' успешно заблокирован');
                res.redirect('/admin/users');

            });

        } else {
            res.render404();
        }

    });

});

module.exports = router;