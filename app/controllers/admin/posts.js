
"use strict";

var acl         = require('../../../config/acl'),
    router      = require('express').Router(),
    mongoose    = require('mongoose'),
    logger      = require('winston'),
    Post        = mongoose.model('Post');

// Если id корректный - передать, если нет - к следующему роуту
router.param('id', function (req, res, next, id) {
    next(mongoose.Types.ObjectId.isValid(id) ? null : 'route');
});


router.get('/', function (req, res) {

    Post.find().sort('-dateCreated').exec(function (err, posts) {
        if ( err ) { throw err; }

        res.render('admin/posts/index', {
            posts: posts
        });

    });

});

router.get('/new', acl.check('post', ['view', 'edit']), function (req, res) {

    res.render('admin/posts/edit');

});

router.get('/:id/edit', acl.check('post', ['view', 'edit']), function (req, res, next) {

    if ( mongoose.Types.ObjectId.isValid(req.params.id) ) {

        Post.findById(req.params.id, function(err, post) {
            if ( err ) { throw err; }

            res.render('admin/posts/edit', {
                post: post
            });
        });

    } else { next('route'); }

});

router.post('/:id', acl.check('post', ['view', 'edit']), function (req, res) {

    Post.findById(req.params.id, function (err, post) {
        if ( err ) { throw err; }

        if ( post ) {

            post.setTitle(req.body.title);
            post.setText(req.body.text);
            post.setActive(req.body.active);

            post.save(function (err, post) {
                if ( err ) { throw err; }

                req.flash('info', 'Новость ' + post.getTitle() + ' успешно обновлена');
                res.redirect('/admin/posts');

            });

        } else { res.render404(); }


    });

});

router.post('/', acl.check('post', ['view', 'edit']), function (req, res) {

    var post = new Post();

    post.setTitle(req.body.title);
    post.setText(req.body.text);
    post.setActive(req.body.active);
    post.setUserId(req.user.getId());

    post.save(function (err, post) {
        if ( err ) { throw err; }

        req.flash('info', 'Новость ' + post.getTitle() + ' успешно создана');
        res.redirect('/admin/posts');

    });

});


router.get('/:id/publish', acl.check('post', ['view', 'edit']), function (req, res) {

    Post.findById(req.params.id, function (err, post) {
        if ( err ) { throw err; }

        if ( post ) {

            post.setActive(true);

            post.save(function (err, post) {

                req.flash('info', 'Новость ' + post.getTitle() + ' успешно опубликована');
                res.redirect('/admin/posts');

            });

        } else {
            res.render404();
        }

    });

});

router.get('/:id/unpublish', acl.check('post', ['view', 'edit']), function (req, res) {

    Post.findById(req.params.id, function (err, post) {
        if ( err ) { throw err; }

        if ( post ) {

            post.setActive(false);

            post.save(function (err, post) {

                req.flash('info', 'Новость ' + post.getTitle() + ' успешно снята с публикации');
                res.redirect('/admin/posts');

            });

        } else {
            throw new Error('Задачи с таким id не существует!');
        }

    });

});

module.exports = router;