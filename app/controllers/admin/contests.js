
"use strict";

var acl         = require('../../../config/acl'),
    mongoose    = require('mongoose'),
    User        = mongoose.model('User'),
    Problem     = mongoose.model('Problem'),
    Contest     = mongoose.model('Contest'),
    router      = require('express').Router();

/**
 * Хехей, тащи пол литра, чтобы разобраться здесь =)
 * Проставляет selected если object.id in ids
 * @param objects
 * @param ids
 */
//_self.setSelected = function (objects, ids) {
//
//    _.each(_.filter(objects, function (object) {
//        return !!_.find(ids, function (id) {
//            return object.id === id.toString();
//        });
//    }), function (object) {
//        object.selected = 'selected';
//    });
//
//};


router.get('/', acl.check('contest', 'view'), function (req, res) {

    Contest.find().exec(function (err, contests) {
        if ( err ) { throw err; }

        res.render('admin/contests/index', {
            contests: contests
        });

    });

});

router.post('/', function (req, res) {

    var contest = new Contest();

    contest.setName(req.body.name);
    contest.setStart(req.body.datetimeStart);
    contest.setEnd(req.body.datetimeEnd);

    contest.setProblems(req.body.problems);
    contest.setUsers(req.body.users);
    contest.setAllowRegister(req.body.allowregister);

    contest.save(function (err) {
        if ( err ) { throw err; }

        req.flash('info', 'Контест ' + contest.getName() + ' успешно создан');
        res.redirect('/admin/contests');

    });

});

router.post('/:id', function (req, res) {

    Contest.findById(req.params.id, function (err, contest) {
        if ( err ) { throw err; }

        contest.setName(req.body.name);
        contest.setStart(req.body.datetimeStart);
        contest.setEnd(req.body.datetimeEnd);

        contest.setProblems(req.body.problems);
        contest.setUsers(req.body.users);
        contest.setAllowRegister(req.body.allowregister);

        contest.save(function (err) {
            if ( err ) { throw err; }

            req.flash('info', 'Контест ' + contest.getName() + ' успешно обновлен');
            res.redirect('/admin/contests');

        });

    });

});

router.get('/new', acl.check('contest', ['edit']), function (req, res) {

    Problem.find().exec(function (err, problems) {
        if ( err ) { throw err; }

        User.find().sort('lname').exec(function (err, users) {
            if ( err ) { throw err; }

            res.render('admin/contests/edit', {
                users: users,
                problems: problems
            });

        });
    });

//    _self.setSelected(users, contest.users);
//    _self.setSelected(problems, contest.problems);

});

router.get('/:id/edit', acl.check('contest', ['edit']), function (req, res) {

    Contest.findById(req.params.id, function (err, contest) {
        if ( err ) { throw err; }

        Problem.find().exec(function (err, problems) {
            if ( err ) { throw err; }

            User.find().sort('lname').exec(function (err, users) {
                if ( err ) { throw err; }

                res.render('admin/contests/edit', {
                    contest: contest,
                    users: users,
                    problems: problems
                });

            });
        });
    });

});

module.exports = router;