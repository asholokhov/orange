
"use strict";

var _           = require('underscore'),
    acl         = require('../../../config/acl'),
    moment      = require('moment'),
    mongoose    = require('mongoose'),
    Attempt     = mongoose.model('Attempt'),
    Problem     = mongoose.model('Problem'),
    Contest     = mongoose.model('Contest'),
    router      = require('express').Router(),
    mailer      = require('../../classes/mailer/mailer');

router.get('/', acl.check('contest', 'view'), function (req, res) {

    Contest.find({ end: {$lt: Date.now() } }).exec(function (err, contests) {
        if ( err ) { throw err; }

        res.render('user/contests/index', {
            contests: contests
        });

    });

});


router.get('/:id', acl.check('contest', 'view'), function (req, res) {

    var data = {},
    /* Штраф за не верный отсыл */
        PENALTY = 20;

    Contest.findById(req.params.id).populate('problems').populate('users').lean().exec(function (err, contest) {

        Attempt.find({problem: {$in: contest.problems}, dateCreated: {$gt: contest.start, $lt: contest.end}, user: {$in: contest.users}, ans: { $ne: 7 }}).sort('dateCreated').lean().exec(function (err, attempts) {

            var cells = {},
                countdownTime,
                status = '';

            /* Инициализируем все ячейки */
            _.each(contest.users, function (user) {

                user.acceptedProblems = 0;
                user.totalTime = 0;

                cells[user._id] = {};

                _.each(contest.problems, function (problem) {

                    cells[user._id][problem._id] = {
                        id: problem._id,
                        count: 0,
                        name: problem.name,
                        accepted: false
                    };

                });

            });

            /* Проставляем все значения ячеек */
            _.each(attempts, function (attempt) {

                if ( attempt.ans === 1 && ! cells[attempt.user][attempt.problem].accepted ) {
                    cells[attempt.user][attempt.problem].accepted = true;
                    cells[attempt.user][attempt.problem].time = attempt.dateCreated - contest.start;
                } else {
                    cells[attempt.user][attempt.problem].count++;
                }
            });

            /* Вычисляем итоговое время */
            _.each(contest.users, function (user) {

                _.each(contest.problems, function (problem) {

                    var cell = cells[user._id][problem._id];

                    if ( cell.accepted ) {
                        user.acceptedProblems += 1;
                        user.totalTime += cell.time + cell.count * PENALTY * 1000 * 60;
                        cell.time = moment(moment.duration(cell.time).subtract(moment.duration({ days: 1, hours: 4 })).asMilliseconds()).format('HH:mm:ss');
                    }

                });

                if ( user.totalTime ) {

                    user.totalTimeToDisplay = moment(moment.duration(user.totalTime)
                        .subtract(moment.duration({ days: 1, hours: 4 }))
                        .asMilliseconds()).format( ( user.totalTime > 24 * 3600 * 1000 ) ? 'DD:HH:mm:ss' : 'HH:mm:ss' );
                }

            });

            function leadZero(number, length) {
                while(number.toString().length < length){
                    number = '0' + number;
                }
                return number;
            }


            /* Сортируем победителей наверх */
            contest.users = _.sortBy(contest.users, function (user) {
                var notAccepted = contest.problems.length - user.acceptedProblems;
                return  notAccepted + leadZero(user.totalTime, 15);
            });

            if ( new Date(contest.start).getTime() > Date.now() ) {
                countdownTime = new Date(contest.start).getTime() - Date.now();
                status = 'До начала осталось: ';
            } else if ( new Date(contest.end).getTime() > Date.now() ) {
                countdownTime = new Date(contest.end).getTime() - Date.now();
                status = 'До завершения осталось: ';
            } else {
                countdownTime = 0;
                status = 'Контест окончен!';
            }

            data._ = _;
            data.moment = moment;
            data.contest = contest;
            data.cells = cells;
            data.countdown = moment.duration(countdownTime)
                .subtract(moment.duration({days: 1, hours: 4}))
                .asMilliseconds();
            data.status = status;

            res.render('user/contests/show', data);

        });

    });

});

router.get('/:contestId/problem/:problemId', function(req, res) {

    Problem.findById(req.params.problemId, function(err, problem) {
        if ( err ) { req.handleDBError(err); }

        Attempt.find({ problem: problem.getId(), user: req.user.getId() }).sort('-dateCreated').exec(function (err, attempts) {
            if ( err ) { req.handleDBError(err); }

            res.render('user/problems/show', {
                problem: problem,
                attempts: attempts
            });

        });

    });

});
router.get('/:id/register', acl.check('contest', 'register'), function (req, res) {

    Contest.findById(req.params.id, function (err, contest) {
        if ( err ) { throw err; }

        contest.getUsers().addToSet(req.user.getId());

        contest.save(function (err, contest) {
            if ( err ) { throw err; }

            req.flash('info', 'Вы успешно зарегистрировались на ' + contest.getName());

            res.redirect('/');

            mailer.send('contestRegister', {
                userId: req.user.getId(),
                contest: contest
            });

        });

    });

});

module.exports = router;