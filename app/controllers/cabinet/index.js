
"use strict";

var _        = require('underscore'),
    acl      = require('../../../config/acl'),
    moment   = require('moment'),
    mongoose = require('mongoose'),
    Post     = mongoose.model('Post'),
    Contest  = mongoose.model('Contest'),
    router   = require('express').Router();

router.get('/', acl.check('/cabinet', 'view'), function (req, res) {

    res.render('cabinet/main');

});

module.exports = router;