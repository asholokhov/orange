"use strict";

var utils = require('../classes/utils'),
    mongoose = require('mongoose'),
    modelName = 'Post',
    Schema = mongoose.Schema,
    schema = new Schema({
        title: { type: String, required: true, trim: true },
        text: { type: String, default: '', trim: true },
        user: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
        active: { type: Boolean, default: false },
        deleted: { type: Boolean, default: false },
        dateCreated: { type: Date, default: Date.now }
    }, {
        safe: true,
        strict: true
    });

/* ========================================================================
 * Статические методы
 * ======================================================================== */


/* ========================================================================
 * Кастомные методы
 * ======================================================================== */


schema.methods.setActive = function (active) {
    this.active = !!active;
};

schema.methods.isActive = function () {
    return this.active;
};

schema.methods.setTitle = function (title) {
    this.title = String(title || '').trim();
};

schema.methods.getTitle = function () {
    return this.title;
};

schema.methods.setText = function (text) {
    this.text = String(text || '').trim();
};

schema.methods.getText = function () {
    return this.text;
};

schema.methods.getUserId = function () {
    return String(this.user);
};

schema.methods.setUserId = function (userId) {
    this.user = ( utils.isObjectIdValid(userId) ) ? userId : null;
};

schema.methods.getSiteId = function () {
    return this.site;
};

schema.methods.getId = function () {
    return this._id;
};


mongoose.model(modelName, schema);