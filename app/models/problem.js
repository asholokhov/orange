
"use strict";

var fs = require('fs-extra'),
    exec = require('child_process').exec,
    env = process.env.NODE_ENV || 'development',
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    logger = require('winston'),
    modelName = 'Problem',
    olympFolder = require('../../config/config')[env].olympFolder;

var schema = new Schema({
    name: { type: String, required: true },
    iofilename: { type: String, required: true },
    timeLimit: { type: Number, required: true },
    memoryLimit: { type: Number, required: true },
    text: { type: String },
    active: { type: Boolean, default: false },
    dateCreated: { type: Date, default: Date.now }
}, {
    safe: true,
    strict: true
});

/* ========================================================================
 * Методы
 * ======================================================================== */

schema.methods.getId = function () {
    return String(this._id);
};

schema.methods.setName = function (name) {
    this.name = name;
};

schema.methods.getName = function () {
    return this.name;
};

schema.methods.setIOFilename = function (iofilename) {
    this.iofilename = iofilename;
};

schema.methods.getIOFilename = function () {
    return this.iofilename;
};

schema.methods.setTimeLimit = function (timeLimit) {
    this.timeLimit = parseInt(timeLimit, 10);
};

schema.methods.getTimeLimit = function () {
    return this.timeLimit;
};

schema.methods.setMemoryLimit = function (memoryLimit) {
    this.memoryLimit = parseInt(memoryLimit, 10);
};

schema.methods.getMemoryLimit = function () {
    return this.memoryLimit;
};

schema.methods.setText = function (text) {
    this.text = text;
};

schema.methods.getText = function () {
    return this.text;
};

schema.methods.setActive = function (active) {
    this.active = !!active;
};

schema.methods.isActive = function () {
    return this.active;
};

schema.methods.updateTests = function (testsPath, cb) {

    var self = this,
        problemsDir = olympFolder + '/problems/' + this.getId();

    fs.exists(String(testsPath), function (exists) {

        if ( exists && testsPath ) {

            // удаляем папку со всеми тестами
            fs.remove(problemsDir, function (err) {
                if ( err ) { throw err; }

                // создаем папочку с тестами заного
                fs.mkdirs(problemsDir, function (err) {
                    if ( err ) { throw err; }

                    // разархивируем
                    exec('unzip ' + testsPath + ' -d ' + problemsDir, function (err, stdout, stderr) {
                        if ( err ) { throw err; }

                        logger.info(stdout);

                        cb(stderr ? new Error('Ошибка при разархивировании тестов к задаче ' + self.getName()) + ' ' + stderr : null );

                    });

                });

            });

        } else {

            logger.info('При сохранении задачи нет тестов');
            cb();

        }

    });



};

mongoose.model(modelName, schema);
