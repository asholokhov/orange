
"use strict";

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    modelName = 'User';

/* ========================================================================
 * Схема в БД
 * ======================================================================== */

var schema = new Schema({
    fname: { type: String, trim: true },
    lname: { type: String, trim: true },
    group: { type: String, trim: true },
    email: { type: String, trim: true, required: true, lowercase: true, unique: true },
    pass: { type: String },
    showFio: { type: Boolean },
    admin: { type: Boolean, default: false },
    active: { type: Boolean, default: true },
    dateCreated: { type: Date, default: Date.now }
}, {
    safe: true,
    strict: true
});


/* ========================================================================
 * Статические методы
 * ======================================================================== */

schema.statics.authenticate = function (email, pass, cb) {
    this.findOne({ email: email.toLowerCase(), pass: pass, active: true }, cb);
};

/* ========================================================================
 * Методы объекта
 * ======================================================================== */

schema.methods.getId = function () {
    return String(this._id);
};

schema.methods.getFname = function () {
    return this.fname;
};

schema.methods.setFname = function (fname) {
    this.fname = fname;
};

schema.methods.getLname = function () {
    return this.lname;
};

schema.methods.setLname = function (lname) {
    this.lname = lname;
};

schema.methods.getGroup = function () {
    return this.group;
};

schema.methods.setGroup = function (group) {
    this.group = group;
};

schema.methods.getEmail = function () {
    return this.email;
};

schema.methods.setEmail = function (email) {
    this.email = email;
};

schema.methods.getPass = function () {
    return this.pass;
};

schema.methods.setPass = function (pass) {
    this.pass = pass;
};

schema.methods.getFullName = function () {
    return this.getLname() + ' ' + this.getFname();
};

schema.methods.isActive = function () {
    return this.active;
};

schema.methods.isAdmin = function () {
    return this.admin;
};

schema.methods.setActive = function (active) {
    this.active = !!active;
};

schema.methods.setAdmin = function (admin) {
    this.admin = !!admin;
};



mongoose.model(modelName, schema);
