
"use strict";

var _ = require('underscore'),
    fs = require('fs-extra'),
    path = require('path'),
    exec = require('child_process').exec,
    logger = require('winston'),
    config = require('../../config/config')[process.env.NODE_ENV],
    mongoose = require('mongoose'),
    Problem = mongoose.model('Problem'),
    Attempt = mongoose.model('Attempt');

// Очередь
var queue = [];

function compareNumbers(a, b) {
    return parseInt(b, 10) - parseInt(a, 10);
}

//function pushToQueue() {
//    process.send('push');
//}
//
//function popFromQueue() {
//    process.send('pop');
//}
//
//process.on('message', function (msg) {
//    if ( msg.type === 'checkerQueue' ) {
//        if ( msg.msg === 'push' ) { queue.push(1); }
//        if ( msg.msg === 'pop' ) { queue.pop(1); }
//    }
//});

var Checker = function () {

    var self = this,
        running = false;

    // Параметры проверки. Обновляются при каждом новом чеке задачи
    self.p = {};

    self.runner = 'SIR'; // {SIR|DN}

    self.isRunning = function () {
        return running;
    };

    self.check = function (user, problem, source, cb) {

//        console.log(user, problem, source, cb);

        queue.push({
            user: user,
            problem: problem,
            source: source,
            cb: cb
        });

        self.checkProblem();

    };

    self.setCheckParams = function (params) {

        self.p = params;
        self.p.testDir = path.normalize(config.olympFolder + '/problems/' + params.problem.getId() + '/');
        self.p.compilePath = '/tmp/compile/3/wheezy/';
        self.p.execFile = 'a.out';
        self.p.symlink = params.problem.getIOFilename() + '.in';
        self.p.cb = params.cb;

    };

    self.checkProblem = function () {

        var attempt = new Attempt();

        // определить, работает ли
        // если свободна - начать проверять задачу выставив флаг
        // если занята - забить.
        // после проверки необходимо проверить очередь на наличие задач на проверку

        logger.info('Пришли к очереди. В очереди сейчас ' + queue.length + ' задач.');

        // если свободна
        if ( ! self.isRunning() && queue.length ) {

            self.checkStart();

            logger.info('Начинаю проверять ' + self.p.problem.getName());

            attempt.problem = self.p.problem.getId();
            attempt.user = self.p.user.getId();
            attempt.test = 0;
            attempt.ans = 1;

            fs.readFile(self.p.source, { encoding: 'utf-8' }, function (err, source) {
                if ( err ) { throw err; }

                logger.info('Прочел файл с кодом');

                attempt.source = source;

                fs.ensureDir(self.p.compilePath, function (err) {
                    if ( err ) { throw err; }

                    var execString = 'g++ -O ' + self.p.source + ' -o ' + self.p.compilePath + self.p.execFile;

                    logger.info('Компилирую: ' + execString);

                    // компилируем
                    exec(execString, function (err, stdout, stderr) {
                        if ( err ) {
                            console.log(err);
                            attempt.ans = 5;
                            attempt.save(self.checkEnd);
                            logger.error('Не могу скомпилировать! "' + execString + '" ' + self.p.problem.getName());
                        } else {

                            // пытаемся прочесть входные параметры
                            fs.readdir(self.p.testDir + 'in/', function (err, files) {

                                if ( err || ! files.length ) {// if problem/problem_id/{in,out} not exists
                                    attempt.ans = 7;
                                    attempt.save(self.checkEnd);
                                    self.p.cb(new Error('Папка "' + self.p.testDir + 'in/" для задачи ' + self.p.problem.getName() + ' отсутствует!'));

                                } else {

                                    files = files.sort(compareNumbers);

                                    self.checkOnTests(files, function (result) {

                                        attempt.ans = result.ans;
                                        attempt.test = result.test;
                                        attempt.save(self.checkEnd);

                                    });

                                }

                            });
                        }

                    });

                });

            });

        }

    };

    self.checkOnTests = function (files, cb) {

        // Получаем тест
        var file = files.pop();

        // Если тесты еще остались - проверяем
        if ( file ) {

            var filename = parseInt(file.substr(0, file.lastIndexOf('.in')), 10);

            logger.info('Добрались до теста ' + filename);

            // Копируем все необходимое в папку для тестов
            fs.copy(self.p.testDir + 'in/' + file, self.p.compilePath + self.p.symlink, function(err) {
                if ( err ) { throw err; }

                var start = new Date().getTime();

                logger.info('Запускаем программу');

                // Запускаем программу
                self['runner' + self.runner](self.p.problem.getTimeLimit(), self.p.problem.getMemoryLimit(), function (err, result) {
                    if ( err ) { throw err; }

                    logger.info('Время выполнения: ' + (new Date().getTime() - start) + '. result: ' + result);

                    // Если программа корректно выполнилась - проверяем результат
                    if ( result === 'OK' ) {

                        var execString = 'diff -B -w ' + self.p.compilePath + self.p.problem.getIOFilename() + '.out ' + self.p.testDir + 'out/' + filename + '.out';

                        logger.info('Запускаю diff: ' + execString);

                        exec(execString, function (err, stdout, stderr) {

                            // Если дифф нашел различия - WA
                            if ( stdout ) {

                                logger.info(stdout);
                                cb({ test: filename, ans: 2 });

                            } else { // Если все ок - следующий тест
                                self.checkOnTests(files, cb);
                            }

                        });

                    } else { // Если run.stdout не ок.
                        cb({ test: filename, ans: self.translateErrResult(result) });
                    }

                });
            });

        } else { // Если тесты кончились и все ок - Accepted
            cb({ test: 0, ans: 1 });
        }

    };

    self.checkStart = function () {

        logger.info('Начал проверять');
        // Флаг, оповещающий о занятости очереди.
        running = true;

        // Устанавливаем параметры на проверку
        self.setCheckParams(queue.pop());

    };

    self.checkEnd = function (err) {

        // Вызываем callback
        self.p.cb(err);

        logger.info('Закончил проверять');

        // Очередь освободилась
        running = false;

        // Если есть в очереди задачи - почекать.
        if ( queue.length ) {
            self.checkProblem();
        }

    };

    self.runnerDN = function (timeLimit, memoryLimit, cb) {
        exec(config.root + '/support/checker3.sh' + ' ' + timeLimit * 1000 + ' ' + memoryLimit, cb);
    };

    self.runnerSIR = function (timeLimit, memoryLimit, cb) {

        require('child_process').execFile(self.p.compilePath + self.p.execFile, { timeout: timeLimit * 1000, cwd: self.p.compilePath }, function (err, stdout, stderr) {

            if ( err ) {

                logger.error(err);

                if ( err.killed === false && err.signal === 'SIGSEGV' ) {
                    cb(null, 'SF');
                } else if ( err.killed === true && err.signal === 'SIGTERM' ) {
                    cb(null, 'TL');
                } else {
                    cb(err);
                }

            } else {

                if ( ! stdout && ! stderr ) {
                    cb(null, 'OK');
                } else {
                    cb(new Error('Не ок при проверке, но и без ошибки'));
                }

            }

        });

    };

    self.translateErrResult = function (result) {
        var ans = 0;

        console.log('result: ', result);

        switch ( result ) {
            case 'UE':
            case 'ERR':
                ans = 7;
                break;
            case 'TL':
                ans = 3;
                break;
            case 'RE':
            case 'SF':
                ans = 6;
                break;
        }

        return ans;
    };

    return self;

};

module.exports = new Checker();


/*
 0  Баг в системе. Переменная не присвоена
 1  Accepted code
 Это значит "Ваше решение верно!!!". Вам действительно удалось решить эту задачу или El Judge ошибся в вашу пользу.
 2  Wrong answer
 Ваша программа, по крайней мере, на одном из тестов выдает неправильный ответ. Номер этого теста прилагается.
 Все тесты с меньшим порядковым номером пройдены успешно.
 3  Time Limit
 Ваша программа, по крайней мере, на одном из тестов работает дольше, чем можно. Ммаксимальное время работы на одном
 тесте указано в условии задачи, по умолчанию оно равно 5 секунд. Номер теста, на котором впервые превышено это время, прилагается.
 4  Presentation error
 Ваша программа, по крайней мере, на одном из тестов выводит данные, формат которых не соответствует формату выходных данных, указанных в задаче.
 Номер этого теста прилагается. Все тесты с порядковым номером меньше этого теста пройдены с положительным результатом.
 Например, требуется вывести какое то число, а ваша программа на этом тесте вывела латинские буквы.
 5  Compilation error
 Ваша программа не компилируется. Это может быть вызвано следующими причинами:
 в Вашей программе ошибка,  вы выбрали не тот язык, когда посылали решение,
 вы используете компилятор, отличный от того, который использует El Judge.
 6  Runtime error
 Ваша программа вернула не нулевой код возврата. Это может вызвано одной из двух причин:
 Ваша программа выполнила недопустимую операцию во время выполнения,
 Ваша программа написана на C/C++ и Вы заканчиваете выполнение функции main с кодом возврата не равным 0.
 7  System error   Эта ошибка не должна появлятся. Она означает, что во время проверки Вашего решения в системе
 возникли какие-то ошибки (система не может найти необходимых файлов, или при ыполнении проверяющей программы возникла ошибка исполнения).
 Кроме того, в случае ElJudge, она может означать Memory Limit Exceeded.
 */

