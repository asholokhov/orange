"use strict";

module.exports = function (app, g) {

    var should = require('should');

    describe('GET /posts/:id ', function () {

        it('Должна дать 200 пользователю', function (done) {
            g.orangeUser
                .get('/posts/' + g.post.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/posts/' + g.post.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 200 гостю', function (done) {
            g.orangeGuest
                .get('/posts/' + g.post.getId())
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

    });



};