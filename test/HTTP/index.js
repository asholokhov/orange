"use strict";

module.exports = function (app, g) {

    var should = require('should'),
        request = require('supertest'),
        agent = request.agent;

    g.orangeAdmin = agent(app);
    g.orangeUser = agent(app);
    g.orangeGuest = agent(app);

    before(function (done) {
        g.orangeAdmin
            .post('/login')
            .send({ email: g.adminEmail, pass: g.adminPass })
            .expect(302)
            .end(function (err) {
                g.orangeUser
                    .post('/login')
                    .send({ email: g.userEmail, pass: g.userPass })
                    .expect(302)
                    .end(done);
            });
    });


    describe('Admin', function () {

        describe('Index', function () { require('./admin/index')(app, g); });
        describe('Problem', function () { require('./admin/problem')(app, g); });
        describe('Contest', function () { require('./admin/contest')(app, g); });
        describe('Post', function () { require('./admin/post')(app, g); });
        describe('User', function () { require('./admin/user')(app, g); });

    });

    describe('Client', function () {

        describe('Index', function () { require('./user/index')(app, g); });
        describe('Problem', function () { require('./user/problem')(app, g); });
        describe('Post', function () { require('./user/post')(app, g); });
        describe('Contest', function () { require('./user/contest')(app, g); });

    });

    describe('Cabinet', function () {

        describe('Index', function () { require('./cabinet/index')(app, g); });

    });

};