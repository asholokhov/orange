"use strict";

module.exports = function (app, g) {

    var should = require('should');

    describe('GET /cabinet ', function () {

        it('Должна дать 200 пользователю', function (done) {
            g.orangeUser
                .get('/cabinet')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 200 админу', function (done) {
            g.orangeAdmin
                .get('/cabinet')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(200)
                .end(done);
        });

        it('Должна дать 302 гостю', function (done) {
            g.orangeGuest
                .get('/cabinet')
                .set('X-Requested-With', 'XMLHttpRequest')
                .expect(302)
                .end(done);
        });

    });

};