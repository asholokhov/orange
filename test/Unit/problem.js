"use strict";

var mongoose = require('mongoose'),
    Problem = mongoose.model('Problem');

module.exports = function (app, global) {

    describe('#getId()', function () {
        it('должен быть', function (done) {
            new Problem().should.have.property('getId');
            new Problem().getId.should.be.type('function');
            done();
        });
    });

    describe('#getName()', function () {
        it('должен быть', function (done) {
            new Problem().should.have.property('getName');
            new Problem().getName.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newProblem = new Problem(),
                fname = 'fname';

            newProblem.setName(fname);
            newProblem.getName().should.be.equal(fname);
            done();
        });
    });

    describe('#setName()', function () {
        it('должен быть', function (done) {
            new Problem().should.have.property('setName');
            new Problem().setName.should.be.type('function');
            done();
        });
    });

    describe('#getIOFilename()', function () {
        it('должен быть', function (done) {
            new Problem().should.have.property('getIOFilename');
            new Problem().getIOFilename.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newProblem = new Problem(),
                iofilename = 'iofilename';

            newProblem.setIOFilename(iofilename);
            newProblem.getIOFilename().should.be.equal(iofilename);
            done();
        });
    });

    describe('#setIOFilename()', function () {
        it('должен быть', function (done) {
            new Problem().should.have.property('setIOFilename');
            new Problem().setIOFilename.should.be.type('function');
            done();
        });
    });

    describe('#getTimeLimit()', function () {
        it('должен быть', function (done) {
            new Problem().should.have.property('getTimeLimit');
            new Problem().getTimeLimit.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newProblem = new Problem(),
                timeLimit = 1;

            newProblem.setTimeLimit(timeLimit);
            newProblem.getTimeLimit().should.be.equal(timeLimit);
            done();
        });
    });

    describe('#setTimeLimit()', function () {
        it('должен быть', function (done) {
            new Problem().should.have.property('setTimeLimit');
            new Problem().setTimeLimit.should.be.type('function');
            done();
        });
    });

    describe('#getMemoryLimit()', function () {
        it('должен быть', function (done) {
            new Problem().should.have.property('getMemoryLimit');
            new Problem().getMemoryLimit.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newProblem = new Problem(),
                memoryLimit = 64;

            newProblem.setMemoryLimit(memoryLimit);
            newProblem.getMemoryLimit().should.be.equal(memoryLimit);
            done();
        });
    });

    describe('#setMemoryLimit()', function () {
        it('должен быть', function (done) {
            new Problem().should.have.property('setMemoryLimit');
            new Problem().setMemoryLimit.should.be.type('function');
            done();
        });
    });

    describe('#getText()', function () {
        it('должен быть', function (done) {
            new Problem().should.have.property('getText');
            new Problem().getText.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newProblem = new Problem(),
                text = 'text of problem';

            newProblem.setText(text);
            newProblem.getText().should.be.equal(text);
            done();
        });
    });

    describe('#setText()', function () {
        it('должен быть', function (done) {
            new Problem().should.have.property('setText');
            new Problem().setText.should.be.type('function');
            done();
        });
    });

    describe('#isActive()', function () {
        it('должен быть', function (done) {
            new Problem().should.have.property('isActive');
            new Problem().isActive.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newProblem = new Problem(),
                active = true;

            newProblem.setActive(active);
            newProblem.isActive().should.be.equal(active);
            done();
        });
    });

    describe('#setActive()', function () {
        it('должен быть', function (done) {
            new Problem().should.have.property('setActive');
            new Problem().setActive.should.be.type('function');
            done();
        });
    });

};