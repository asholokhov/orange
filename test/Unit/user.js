"use strict";

var mongoose = require('mongoose'),
    User = mongoose.model('User');

module.exports = function (app, global) {

    describe('#getId()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('getId');
            new User().getId.should.be.type('function');
            done();
        });
    });

    describe('#getFname()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('getFname');
            new User().getFname.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newUser = new User(),
                fname = 'fname';

            newUser.setFname(fname);
            newUser.getFname().should.be.equal(fname);
            done();
        });
    });


    describe('#setFname()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('setFname');
            new User().setFname.should.be.type('function');
            done();
        });
    });

    describe('#getGroup()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('getGroup');
            new User().getGroup.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newUser = new User(),
                fname = 'fname';

            newUser.setGroup(fname);
            newUser.getGroup().should.be.equal(fname);
            done();
        });
    });

    describe('#setGroup()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('setGroup');
            new User().setGroup.should.be.type('function');
            done();
        });
    });

    describe('#getPass()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('getPass');
            new User().getPass.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newUser = new User(),
                fname = 'fname';

            newUser.setPass(fname);
            newUser.getPass().should.be.equal(fname);
            done();
        });
    });

    describe('#setPass()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('setPass');
            new User().setPass.should.be.type('function');
            done();
        });
    });

    describe('#getEmail()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('getEmail');
            new User().getEmail.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newUser = new User(),
                fname = 'fname';

            newUser.setEmail(fname);
            newUser.getEmail().should.be.equal(fname);
            done();
        });
    });

    describe('#setEmail()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('setEmail');
            new User().setEmail.should.be.type('function');
            done();
        });
    });

    describe('#getLname()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('getLname');
            new User().getLname.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newUser = new User(),
                lname = 'lname';

            newUser.lname = lname;
            newUser.getLname().should.be.equal(lname);
            done();
        });
    });
    
    describe('#getFullName()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('getFullName');
            new User().getFullName.should.be.type('function');
            done();
        });
    });

    describe('#setLname()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('setLname');
            new User().setLname.should.be.type('function');
            done();
        });
    });
    
    describe('#isActive()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('isActive');
            new User().isActive.should.be.type('function');
            done();
        });
    });
    
    describe('#setActive()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('setActive');
            new User().setActive.should.be.type('function');
            done();
        });
    });
    
    describe('#setAdmin()', function () {
        it('должен быть', function (done) {
            new User().should.have.property('setAdmin');
            new User().setAdmin.should.be.type('function');
            done();
        });
    });

};