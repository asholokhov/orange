"use strict";

var mongoose = require('mongoose'),
    moment = require('moment'),
    Contest = mongoose.model('Contest');

module.exports = function (app, g) {

    describe('#getId()', function () {
        it('должен быть', function (done) {
            new Contest().should.have.property('getId');
            new Contest().getId.should.be.type('function');
            done();
        });
    });

    describe('#getName()', function () {
        it('должен быть', function (done) {
            new Contest().should.have.property('getName');
            new Contest().getName.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newContest = new Contest(),
                fname = 'fname';

            newContest.setName(fname);
            newContest.getName().should.be.equal(fname);
            done();
        });
    });

    describe('#setName()', function () {
        it('должен быть', function (done) {
            new Contest().should.have.property('setName');
            new Contest().setName.should.be.type('function');
            done();
        });
    });

    describe('#getText()', function () {
        it('должен быть', function (done) {
            new Contest().should.have.property('getText');
            new Contest().getText.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newContest = new Contest(),
                text = 'text of contest';

            newContest.setText(text);
            newContest.getText().should.be.equal(text);
            done();
        });
    });

    describe('#setText()', function () {
        it('должен быть', function (done) {
            new Contest().should.have.property('setText');
            new Contest().setText.should.be.type('function');
            done();
        });
    });


    describe('#getStart()', function () {
        it('должен быть', function (done) {
            new Contest().should.have.property('getStart');
            new Contest().getStart.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {

            var newContest = new Contest(),
                date = moment().format('DD.MM.YYYY HH:mm:ss');

            newContest.setStart(date);

            moment(newContest.getStart()).valueOf().should.be.equal(moment(date, 'DD.MM.YYYY HH:mm:ss').valueOf());

            done();

        });
    });

    describe('#setStart()', function () {
        it('должен быть', function (done) {
            new Contest().should.have.property('setStart');
            new Contest().setStart.should.be.type('function');
            done();
        });
    });



    describe('#getEnd()', function () {
        it('должен быть', function (done) {
            new Contest().should.have.property('getEnd');
            new Contest().getEnd.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {

            var newContest = new Contest(),
                date = moment().format('DD.MM.YYYY HH:mm:ss');

            newContest.setEnd(date);
            moment(newContest.getEnd()).valueOf().should.be.equal(moment(date, 'DD.MM.YYYY HH:mm:ss').valueOf());

            done();

        });
    });

    describe('#setEnd()', function () {
        it('должен быть', function (done) {
            new Contest().should.have.property('setEnd');
            new Contest().setEnd.should.be.type('function');
            done();
        });
    });

    describe('#getProblems()', function () {
        it('должен быть', function (done) {
            new Contest().should.have.property('getProblems');
            new Contest().getProblems.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {
            var newContest = new Contest();

            newContest.setProblems(g.problem.getId());
            String(newContest.getProblems()[0]).should.be.equal(g.problem.getId());
            done();
        });
    });

    describe('#setProblems()', function () {
        it('должен быть', function (done) {
            new Contest().should.have.property('setProblems');
            new Contest().setProblems.should.be.type('function');
            done();
        });
    });


    describe('#getUsers()', function () {

        it('должен быть', function (done) {
            new Contest().should.have.property('getUsers');
            new Contest().getUsers.should.be.type('function');
            done();
        });

        it('Должен вернуть значение', function (done) {

            var newContest = new Contest();

            newContest.setUsers(g.user.getId());
            String(newContest.getUsers()[0]).should.be.equal(g.user.getId());

            done();

        });
    });

    describe('#setUsers()', function () {
        it('должен быть', function (done) {
            new Contest().should.have.property('setUsers');
            new Contest().setUsers.should.be.type('function');
            done();
        });
    });

};