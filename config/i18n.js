/**
 * Created by ivan on 7/17/14.
 */

var i18n = require('i18n');

i18n.configure({
    locales:['ru', 'en'],
    directory: __dirname + '/locales',
    defaultLocale: 'ru'
});