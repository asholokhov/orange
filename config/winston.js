
"use strict";

var env = process.env.NODE_ENV,
    winston = require('winston'),
    MongoDB = require('winston-mongodb').MongoDB,
    Mail = require('winston-mail').Mail,
    config = require('./config')[env];

var mongoOptions = {
    level: 'warn',
    db: config.db.dbName,
    host: config.db.host,
    username: config.db.user,
    password: config.db.pass
};

var mailOptions = {
    level: 'error',
    to: config.mail.to,
    from: config.mail.from,
    host: config.mail.host,
    port: config.mail.port,
    username: config.mail.from,
    password: config.mail.pass,
    tls: config.mail.tls
};

var fileOptions = {
    level: 'info',
    timestamp: true,
    filename: '/var/log/orange/logger.log',
    maxsize: 100
};

//winston.add(winston.transports.File, fileOptions);

if ( env === 'production' ) {
    winston.add(MongoDB, mongoOptions);
    winston.add(Mail, mailOptions);
}