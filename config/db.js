"use strict";

var mongoose = require('mongoose'),
    registry = require('../app/classes/registry'),
    config = require('./config')[registry.get('env') || 'development'],
    logger = require('winston'),
    fs = require('fs'),
    db = mongoose.connection,
    modelsPath = __dirname + '/../app/models',
    dbUri = config.db.link + config.db.dbName,
    mongooseOptions = {
        server: {
            auto_reconnect:true
        },
        user: config.db.user,
        pass: config.db.pass
    };

mongoose.connect(dbUri, mongooseOptions);

fs.readdirSync(modelsPath).forEach(function (file) {
    require(modelsPath + '/' + file);
});

db.on('error', function(err) {
    logger.error('Error in MongoDb connection: ' + err.stack);
    mongoose.disconnect();
});

db.on('disconnected', function() {
    logger.warn('MongoDB disconnected!');
    setTimeout(function () { mongoose.connect(dbUri, mongooseOptions); }, 5000);
});

db.once('open', function () {
    logger.info('connected to DB');
});