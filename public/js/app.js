
$(document).ready(function () {

    "use strict";

    //------------------------------------------------------------------
    //
    //  для активации кнопок меню
    //
    $('.navbar-inner .nav a').each(function () {
        if ( this.pathname === document.location.pathname ) {
            $(this).parent().addClass("active");
        }
    });

    //------------------------------------------------------------------
    //
    //  Для пагинации
    //
    // TODO: поглядеть что за функция такая. Но написано достаточно много для тупой пагинации.
    (function activatePaginationButtons() {
        var pathname = document.location.pathname,
            delimIndex = pathname.lastIndexOf('/')+1,
            page = parseInt(pathname.substr(delimIndex), 10) || 1,
            pagination = $('.pagination a'),
            notNum = isNaN( parseInt(pathname.substr(delimIndex), 10) );

        if ( page > pagination.size() - 3 ) {
            pagination.last().parent().addClass("disabled");
        } else {
            pagination.last().attr({
                "href": pathname.substr(0, delimIndex) + (page + 1).toString()
            });
        }

        if ( page < 2 ) {
            pagination.first().parent().addClass("disabled");
            if (notNum) {
                pagination.last().attr({
                    "href": pathname + '/2'
                });
            } else {
                pagination.last().attr({
                    "href": pathname.substr(0, delimIndex) + (page + 1).toString()
                });
            }
        } else {
            pagination.first().attr({
                "href": pathname.substr(0, delimIndex) + (page - 1).toString()
            });
        }
        pagination.eq(page).parent().addClass("active");
    }());

    $('#datetimeStart').datetimepicker();
    $('#datetimeEnd').datetimepicker();

    $('.chosen-select').chosen();

    /* Показать/скрыть исходник */

    $('.btn-show-source').click(function () {
        var sourceTr = $(this).closest('tr').next();
        sourceTr.toggle();
        if( sourceTr.is(':visible') ) {
            $(this).text('Кратко');
        } else {
            $(this).text('Подробно');
        }
    });

    /* Highlight all available sources */
    SyntaxHighlighter.all();

    tinymce.init({
        plugins: "table",
        tools: "inserttable",
        selector:'textarea'
    });

    $('[data-countdown]').each(function() {
        var $this = $(this), finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function(event) {
            //%D %!D:день,дня;
            $this.html(event.strftime(event.offset.totalDays * 24 + event.offset.hours + ':%M:%S'));
        });
    });

});
