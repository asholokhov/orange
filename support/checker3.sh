#!/bin/bash
TIME=$1
MEMORY=$2
WORKER_NAME="3"
DIST_NAME="wheezy"
USER="$DIST_NAME$WORKER_NAME"
#chroot --userspec checker:checker /sandbox/1/wheezy/ bash /tmp/env.sh /tmp/exec/a.out $TIME $MEMORY  2>/dev/null
sudo chroot --userspec $USER:$USER "/sandbox/$WORKER_NAME/$DIST_NAME" bash /tmp/env.sh /tmp/exec/a.out $TIME $MEMORY
if [ $? -ne 0 ];then 
  echo -n "ERR" 
fi
