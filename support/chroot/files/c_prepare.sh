#!/bin/bash
# prepare chroot environment

USERNAME=$1
GROUPNAME=$2
CHECKER_UID=$3
CHECKER_GID=$3
EXEC_PATH='/tmp/exec'
HISTORY_PATH='/tmp/history'

msg() {
  echo "Chroot: "$1
}

user_prepare() {
  msg "Prepare user!"
  groupadd -g $CHECKER_GID $GROUPNAME
  useradd -mb /home -g $GROUPNAME -u $CHECKER_UID -s /bin/bash $USERNAME
}

folders_prepare() {
  msg "Folders prepare!"
  mkdir -p $EXEC_PATH
  mkdir -p $HISTORY_PATH
  chown $USERNAME:$GROUPNAME -R $EXEC_PATH
  chown $USERNAME:$GROUPNAME -R $HISTORY_PATH
  chmod g+w $EXEC_PATH
  chmod g+w $HISTORY_PATH
}


user_prepare
folders_prepare
