#!/bin/bash
#orange.sh: a orange daemon

PAUSE=5
ORANGE_SERVICE=orange
NAME=orange
PIDFILE=/var/run/orange.pid
EXEC=/usr/local/bin/node

APP_PATH=/srv/orange
APP_FILE=server.js
APP_OPTIONS=$APP_PATH/$APP_FILE 

DAEMON_LOG=/var/log/orange/daemon.log
STDOUT_LOG=/var/log/orange/stdout.log
STDERR_LOG=/var/log/orange/stderr.log

# -S - start
# -b - background
# -q - quiet. Do not print info messages
# -m - make pid
# -p - pid
# -x - exec
# -c - change to username
# parametrs to $EXEC after --

DAEMON_START_OPTIONS="-S -b -mp $PIDFILE -d $APP_PATH --name $NAME -c node -g node --no-close "

get_time() {
        echo `date +"%F %H:%M:%S"`
}

start_server() {
    NODE_ENV=production start-stop-daemon $DAEMON_START_OPTIONS --exec $EXEC -- $APP_OPTIONS > $STDOUT_LOG 2>$STDERR_LOG
    echo "$(get_time): [SVC] Server started with PID `cat $PIDFILE`" >> $DAEMON_LOG;
}

while :
do
    if [ -f $PIDFILE ]
    then
        if ! ps hf --pid `cat $PIDFILE` > /dev/null
        then 
            echo "$(get_time): [SVC] Server with PID `cat $PIDFILE` is down!" >> $DAEMON_LOG;      
            rm -f $PIDFILE > /dev/null
            start_server  
        fi
    else
        echo "$(get_time): [SVC] Server is down!" >> $DAEMON_LOG;
        start_server
    fi
    sleep ${PAUSE}
done

