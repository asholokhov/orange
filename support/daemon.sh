#!/bin/bash
PIDFILE=/var/run/orange.pid
SRV_PATH=/srv/app
APP=server.js
OPTIONS=$SRV_PATH/$APP
EXEC=/usr/local/bin/node
NAME=orange
DAEMON_LOG=/var/log/orange/daemon.log
SERVICE=/etc/service/$NAME

# tmp paths for chroot
TMP_PATH=/tmp/exec
HISTORY_PATH=/tmp/history

msg_info() {
  echo "$(get_time): [I] $1" >> $DAEMON_LOG
}

msg_error() {
  echo "$(get_time): [E] $1" >> $DAEMON_LOG
}

start_server() {
	echo "Node server starting"
	if [ ! -f $PIDFILE ] 
	then
		svc -u $SERVICE
		sleep 6
		if ps hf --pid `cat $PIDFILE` > /dev/null
		then
		    msg_info "Start server with PID `cat $PIDFILE`"
    		    echo "Start server with PID `cat $PIDFILE`"
 		    #if [ ! -d $TMP_PATH ]
		    #then
		#	mkdir -p $TMP_PATH
		#    fi
		else
		msg_error "Failed to start server!"
    		echo "Failed to start server!"
		fi
	else
		
		if ps hf --pid `cat $PIDFILE` > /dev/null
		then # do it normal!!!
			msg_info "Server is already running with PID `cat $PIDFILE`"
			echo " Server is already running with PID `cat $PIDFILE`"
	
		else
			msg_info "Something is wrong with PID `cat $PIDFILE`. Could not start server!"
			echo "Something is wrong with PID `cat $PIDFILE`. Could not start server!"
			rm -f $PIDFILE > /dev/null
			# start_server
		fi
	fi
}

stop_server() {
	echo "Node server stopping"
	
	if [ -f $PIDFILE ] 
	then
		svc -d $SERVICE
		if start-stop-daemon -Kqp $PIDFILE; 
		then
			msg_info "Stop server with PID `cat $PIDFILE`"
			echo "Stop server with PID `cat $PIDFILE`"
			rm -f $PIDFILE
		else
			msg_error "Failed to stop server with PID `cat $PIDFILE`!"
			echo "Failed to stop server with PID `cat $PIDFILE`!"
		fi
	else
		echo "Could not stop server! Server is not running!"
	fi
}

restart_server() {
	echo "Node server restart"
	stop_server
	sleep 3 
	start_server
}
server_status() {
	if [ -f $PIDFILE ]
	then
# second way	if cat /var/run/server.pid | xargs ps hf --pid $1 > /dev/null
		if ps hf --pid `cat $PIDFILE` > /dev/null
		then
			echo "Server is running! PID `cat $PIDFILE`"
			echo `svstat $SERVICE`
#			ps f --pid `cat $PIDFILE`
		else
			echo "Server fell down! Some bug has occurred! Take action!";
		fi
	else
		echo "Server is not running"
fi
}
full_server_status() {
	[ -f $PIDFILE ] && lsof -p `cat $PIDFILE` || echo "$(get_time) Not running!"
}

delete_pid() {
	if [ -f $PIDFILE ]; then		
		msg_info "Delete PID file"
		rm -f $PIDFILE
	fi
}

get_time() {
	echo `date +"%F %H:%M:%S"`
}

case "$1" in 
	start)
	start_server
	;;
	stop)
	stop_server
	;; 
	restart)
	restart_server
	;;
	status)
	server_status
	;;
	full)
	full_server_status
	;;
*)
echo "usage start|stop|restart|status|full"
;;
esac
exit 0

