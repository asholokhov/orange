#!/bin/bash
DATE=`date +%F`
DATETIME=`date +%F_%H%M`
FOLDER=/home/backup/$DATE
if [ ! -d $FOLDER ]; then
  mkdir -p $FOLDER
fi

cd $FOLDER
if [ ! -d "$FOLDER/night" ]; then
  mkdir $FOLDER/night
fi

cd $FOLDER/night

/etc/init.d/orange stop && /etc/init.d/mongodb stop 
mongodump --dbpath /var/lib/mongodb -o /home/backup/mongo_all 
sleep 2
tar cjf mongo.tar.gz /home/backup/mongo_all
rm -rf /home/backup/mongo_all

/etc/init.d/mongodb start && /etc/init.d/orange start 
tar cjf app.tar.gz /srv/app
tar cjf git.tar.gz /srv/repos
tar cjf secure.tar.gz /home/git/.ssh/authorized_keys /root/.ssh/ /srv/certs

tar cjf backup_$DATETIME.tar.gz {app,git,secure,mongo}.tar.gz

rm {app,git,secure,mongo}.tar.gz
