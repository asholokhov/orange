#!/bin/bash
	
FOLDER=/home/backup
cd $FOLDER

find . -type f -ctime 2 -name hour_*.tar.gz  | awk '{ if (NR % 2 == 0) print $0 }' | xargs rm
find . -type f -ctime +3 -name hour_*.tar.gz | xargs rm
find . -type f -ctime +30
