#!/bin/bash
chown www-data:www-data -R /srv/app/olymp/problems
find /srv/app/olymp/problems/* | xargs chmod 775
